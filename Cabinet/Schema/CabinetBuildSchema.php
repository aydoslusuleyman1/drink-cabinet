<?php

namespace Cabinet\Schema;

class CabinetBuildSchema
{
    /** @var string $doorColor */
    protected $doorColor;

    /** @var string $doorStatus */
    protected $doorStatus;

    /** @var array $shelfSchemes */
    protected $shelfSchemes;

    /**
     * @return string
     */
    public function getDoorColor(): string
    {
        return $this->doorColor;
    }

    /**
     * @param string $doorColor
     */
    public function setDoorColor(string $doorColor): void
    {
        $this->doorColor = $doorColor;
    }

    /**
     * @return string
     */
    public function getDoorStatus(): string
    {
        return $this->doorStatus;
    }

    /**
     * @param string $doorStatus
     */
    public function setDoorStatus(string $doorStatus): void
    {
        $this->doorStatus = $doorStatus;
    }

    /**
     * @return array
     */
    public function getShelfSchemes(): array
    {
        return $this->shelfSchemes;
    }

    /**
     * @param array $shelfSchemes
     */
    public function setShelfSchemes(array $shelfSchemes): void
    {
        $this->shelfSchemes = $shelfSchemes;
    }
}