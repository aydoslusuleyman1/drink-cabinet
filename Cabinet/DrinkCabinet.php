<?php

namespace Cabinet;

use Cabinet\Component\Door;
use Cabinet\Component\Shelf;
use Cabinet\Drink\Drink;

class DrinkCabinet implements \JsonSerializable
{
    const SHELF_LIMIT = 3;
    const MAX_DRINK_COUNT_BY_SHELF = 20;
    const DRINK_SIZE = 33;
    const DRINK_NAME = 'COCA COLA';
    const DRINK_TYPE = 'CANNED';

    protected $lockStatus = false;

    protected $state = NULL;

    protected $drinks = [];

    protected $shelves = [];

    /** @var Door $door */
    protected $door;

    /**
     * @return mixed
     */
    public function getState()
    {
        if (count($this->drinks) == $this->getDrinkCapacity()) {
            return "FULL";
        } else if (count($this->drinks) >= $this->getDrinkCapacity() * 0.5) {
            return "PARTLY FULL";
        } else if (count($this->drinks) <= $this->getDrinkCapacity() * 0.5) {
            return "PARTLY EMPTY";
        } else if (count($this->drinks) == 0) {
            return "EMPTY";
        }

        return null;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getDrinks()
    {
        return $this->drinks;
    }

    /**
     * @param mixed $drinks
     */
    public function setDrinks($drinks)
    {
        $this->drinks = $drinks;
    }

    public function addDrink(Drink $drink)
    {
        if (!count($this->drinks)) {
            $this->drinks[] = $drink;
        } else {
            $this->drinks = array_merge($this->drinks, [$drink]);
        }
    }

    /**
     * @return mixed
     */
    public function getShelves()
    {
        return $this->shelves;
    }

    /**
     * @param $shelves
     */
    public function setShelves($shelves)
    {
        $this->shelves = $shelves;
    }

    public function addShelf(Shelf $shelf)
    {
        // shelf size control
        if ($shelf->getSize() > self::MAX_DRINK_COUNT_BY_SHELF)
            throw new \Exception('max drink count of a shelf is limited by ' . self::MAX_DRINK_COUNT_BY_SHELF . PHP_EOL);

        if (count($this->shelves) > self::SHELF_LIMIT - 1)
            throw new \Exception('max shelf count of a drink cabinet is limited by :' . self::SHELF_LIMIT . PHP_EOL);

        if (!count($this->shelves)) {
            $this->shelves[] = $shelf;
        } else {
            $this->shelves = array_merge($this->shelves, [$shelf]);
        }

        return $this->shelves;
    }

    /**
     * @return Door
     */
    public function getDoor()
    {
        return $this->door;
    }

    /**
     * @param mixed $door
     */
    public function setDoor($door)
    {
        $this->door = $door;
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->lockStatus;
    }

    /**
     * @param bool $lockStatus
     * @return bool
     */
    public function setLockStatus(bool $lockStatus)
    {
        return $this->lockStatus = $lockStatus;
    }

    public function getDrinkCapacity()
    {
        return array_sum(array_map(function (Shelf $shelf) {
            return $shelf->getSize();
        }, $this->shelves));
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'doorState' => $this->getDoor()->getState(),
            'shelves' => $this->getShelves(),
            'drinks' => $this->getDrinks(),
            'state' => $this->getState()
        ];
    }
}