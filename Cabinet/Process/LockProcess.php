<?php

namespace Cabinet\Process;

use Cabinet\Drink\Drink;
use Cabinet\DrinkCabinet;

class LockProcess implements LoadChainInterface
{
    /** @var LoadChainInterface */
    private $nextInChain;

    public function handle(DrinkCabinet $drinkCabinet)
    {
        if ($drinkCabinet->isLocked())
            throw new \Exception('This process already doing' . PHP_EOL);

        $drinkCabinet->setLockStatus(true);
        return $drinkCabinet;
    }

    public function setNext(LoadChainInterface $nextInChain)
    {
        $this->nextInChain = $nextInChain;
    }

    /**
     * @inheritDoc
     */
    public function process(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        try {
            $this->handle($drinkCabinet);
            $this->nextInChain->process($drinkCabinet, $drink);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }
}