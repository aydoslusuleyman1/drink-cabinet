<?php


namespace Cabinet\Process;


use Cabinet\Drink\Drink;
use Cabinet\DrinkCabinet;

class LoadDrink implements LoadChainInterface
{
    /** @var LoadChainInterface */
    private $nextInChain;

    public function handle(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        $drinkCabinet->addDrink($drink);

        return $drinkCabinet;
    }

    public function setNext(LoadChainInterface $nextInChain)
    {
        $this->nextInChain = $nextInChain;
    }

    /**
     * @inheritDoc
     */
    public function process(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        $this->handle($drinkCabinet, $drink);
        $this->nextInChain->process($drinkCabinet, $drink);
    }
}