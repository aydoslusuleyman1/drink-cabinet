<?php


namespace Cabinet\Process;


use Cabinet\Drink\Drink;
use Cabinet\DrinkCabinet;

class DrinkUnLoader
{
    public function unload(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        $lockLoad = new LockProcess();
        $checkDrinkType = new CheckDrinkType();
        $unloadDrink = new UnLoadDrink();
        $unLockLoad = new UnLockProcess();

        $lockLoad->setNext($checkDrinkType);
        $checkDrinkType->setNext($unloadDrink);
        $unloadDrink->setNext($unLockLoad);


        // trigger first process
        $lockLoad->process($drinkCabinet, $drink);

        return $drinkCabinet;

    }
}