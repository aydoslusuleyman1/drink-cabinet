<?php


namespace Cabinet\Process;


use Cabinet\Drink\Drink;
use Cabinet\DrinkCabinet;

class CheckDrinkType implements LoadChainInterface
{
    /** @var LoadChainInterface */
    private $nextInChain;

    public function handle(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        if ($drink->getSize() != DrinkCabinet::DRINK_SIZE || $drink->getName() != DrinkCabinet::DRINK_NAME || $drink->getType() != DrinkCabinet::DRINK_TYPE)
            throw new \Exception('This drink is not suitable.' . PHP_EOL);

        return $drinkCabinet;
    }

    public function setNext(LoadChainInterface $nextInChain)
    {
        $this->nextInChain = $nextInChain;
    }

    /**
     * @inheritDoc
     */
    public function process(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        try {
            $drinkCabinet = $this->handle($drinkCabinet, $drink);
            $this->nextInChain->process($drinkCabinet, $drink);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            $this->setNext(new UnLockProcess());
            $this->nextInChain->process($drinkCabinet, $drink);
        }
    }
}