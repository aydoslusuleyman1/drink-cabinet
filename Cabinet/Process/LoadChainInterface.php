<?php

namespace Cabinet\Process;

use Cabinet\Drink\Drink;
use Cabinet\DrinkCabinet;

interface LoadChainInterface
{
    public function setNext(LoadChainInterface $nextInChain);

    /**
     * @param DrinkCabinet $drinkCabinet
     * @param Drink $drink
     * @return array
     */
    public function process(DrinkCabinet $drinkCabinet, Drink $drink);
}