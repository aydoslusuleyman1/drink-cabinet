<?php


namespace Cabinet\Process;


use Cabinet\Component\Door;
use Cabinet\Drink\Drink;
use Cabinet\DrinkCabinet;

class OpenDoor implements LoadChainInterface
{
    /** @var LoadChainInterface */
    private $nextInChain;

    public function open(DrinkCabinet $drinkCabinet)
    {
        if ($drinkCabinet->getDoor()->getState() == Door::stateMap['closed']) {
            echo "door is closed, opened now." . PHP_EOL;
            $drinkCabinet->getDoor()->setState(Door::stateMap['open']);
        }

        return $drinkCabinet;
    }

    public function setNext(LoadChainInterface $nextInChain)
    {
        $this->nextInChain = $nextInChain;
    }

    /**
     * @inheritDoc
     */
    public function process(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        $this->open($drinkCabinet);
        $this->nextInChain->process($drinkCabinet, $drink);
    }
}