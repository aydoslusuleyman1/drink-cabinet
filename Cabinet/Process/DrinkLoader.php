<?php

namespace Cabinet\Process;

use Cabinet\Drink\Drink;
use Cabinet\DrinkCabinet;

class DrinkLoader
{
    public function load(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        $lockLoad = new LockProcess();
        $openDoor = new OpenDoor();
        $checkDrinkType = new CheckDrinkType();
        $checkDrinkCount = new CheckDrinkCount();
        $loadDrink = new LoadDrink();
        $unLockLoad = new UnLockProcess();

        $lockLoad->setNext($openDoor);
        $openDoor->setNext($checkDrinkType);
        $checkDrinkType->setNext($checkDrinkCount);
        $checkDrinkCount->setNext($loadDrink);
        $loadDrink->setNext($unLockLoad);

        // trigger first process
        $lockLoad->process($drinkCabinet, $drink);

        return $drinkCabinet;
    }
}