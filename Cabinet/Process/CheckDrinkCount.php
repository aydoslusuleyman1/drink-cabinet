<?php


namespace Cabinet\Process;


use Cabinet\Drink\Drink;
use Cabinet\DrinkCabinet;

class CheckDrinkCount implements LoadChainInterface
{
    /** @var LoadChainInterface */
    private $nextInChain;

    public function handle(DrinkCabinet $drinkCabinet)
    {
        if (count($drinkCabinet->getDrinks()) >= $drinkCabinet->getDrinkCapacity())
            throw new \Exception('Drink Cabinet is Full. You can not add more drinks' . PHP_EOL);

        return $drinkCabinet;
    }

    public function setNext(LoadChainInterface $nextInChain)
    {
        $this->nextInChain = $nextInChain;
    }

    /**
     * @inheritDoc
     */
    public function process(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        try {
            $this->handle($drinkCabinet);
            $this->nextInChain->process($drinkCabinet, $drink);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
            $this->setNext(new UnLockProcess());
            $this->nextInChain->process($drinkCabinet, $drink);
        }
    }
}