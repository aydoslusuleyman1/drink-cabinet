<?php


namespace Cabinet\Process;


use Cabinet\Drink\Drink;
use Cabinet\DrinkCabinet;

class UnLoadDrink implements LoadChainInterface
{
    /** @var LoadChainInterface */
    private $nextInChain;

    public function unload(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        $drinks = $drinkCabinet->getDrinks();

        $key = array_search($drink, $drinks, true);
        unset($drinks[$key]);

        $drinkCabinet->setDrinks($drinks);

        return $drinkCabinet;
    }

    public function setNext(LoadChainInterface $nextInChain)
    {
        $this->nextInChain = $nextInChain;
    }

    /**
     * @inheritDoc
     */
    public function process(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        $this->unload($drinkCabinet, $drink);
        $this->nextInChain->process($drinkCabinet, $drink);
    }
}