<?php

namespace Cabinet\Process;

use Cabinet\Drink\Drink;
use Cabinet\DrinkCabinet;

class UnLockProcess implements LoadChainInterface
{
    /** @var LoadChainInterface */
    private $nextInChain;

    public function handle(DrinkCabinet $drinkCabinet)
    {
        $drinkCabinet->setLockStatus(false);
        return $drinkCabinet;
    }

    public function setNext(LoadChainInterface $nextInChain)
    {
        $this->nextInChain = null;
    }

    /**
     * @inheritDoc
     */
    public function process(DrinkCabinet $drinkCabinet, Drink $drink)
    {
        try {
            $this->handle($drinkCabinet);
        } catch (\Exception $exception) {
            echo $exception->getMessage();
        }
    }
}