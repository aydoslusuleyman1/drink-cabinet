<?php

namespace Cabinet\Component;

class Shelf implements \JsonSerializable
{
    /** @var string $color */
    protected $color;

    /** @var int $size */
    protected $size;

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return int
     */
    public function setSize(int $size): int
    {
        return $this->size = $size;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'size' => $this->getSize(),
            'color' => $this->getColor()
        ];
    }
}