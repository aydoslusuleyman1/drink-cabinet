<?php


namespace Cabinet\Component;


class Door
{
    const stateMap = [
        'open' => 'Door Is Open',
        'barelyOpen' => 'Door Is Barely Open',
        'closed' => 'Door Is Closed'
    ];

    protected $color;

    protected $state;

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     * @return string
     */
    public function setColor($color): string
    {
        return $this->color = $color;
    }

    /**
     * @return string
     */
    public function getState(): string
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @return string
     */
    public function setState($state): string
    {
        return $this->state = $state;
    }
}