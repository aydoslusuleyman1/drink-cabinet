<?php

namespace Cabinet\Builder;

use Cabinet\Component\Door;
use Cabinet\Component\Shelf;
use Cabinet\DrinkCabinet;
use Cabinet\Schema\CabinetBuildSchema;
use Exception;

class CabinetBuilder
{
    public function build(CabinetBuildSchema $cabinetBuildSchema)
    {
        try {
            $cabinet = new DrinkCabinet();

            // building door
            $door = new Door();
            $door->setColor($cabinetBuildSchema->getDoorColor());
            $door->setState($cabinetBuildSchema->getDoorStatus());

            $cabinet->setDoor($door);

            // building shelves
            /** @var \Cabinet\Schema\ShelfBuildSchema $scheme */
            foreach ($cabinetBuildSchema->getShelfSchemes() as $scheme) {

                $shelf = new Shelf();
                $shelf->setSize($scheme->getSize());
                $shelf->setColor($scheme->getColor());

                $cabinet->addShelf($shelf);
            }

            return $cabinet;
        } catch (Exception $exception) {
            echo $exception->getMessage();
            return $cabinet;
        }
    }
}