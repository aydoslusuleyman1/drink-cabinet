<?php

namespace Cabinet\Drink;

class Water extends Drink
{
    public function __construct()
    {
        $this->setName("Hayat Su");
        $this->setSize("100");
        $this->setType("Glass");
    }
}