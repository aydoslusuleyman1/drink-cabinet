<?php

namespace Cabinet\Drink;

abstract class Drink implements \JsonSerializable
{
    const UNIT = 'CL';

    protected $name;

    protected $size;

    protected $type;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return string
     */
    public function setName($name): string
    {
        return $this->name = $name;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return int
     */
    public function setSize($size): int
    {
        return $this->size = $size;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return string
     */
    public function setType($type): string
    {
        return $this->type = $type;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'size' => sprintf('%d %s', $this->getSize(), self::UNIT),
            'type' => $this->getType()
        ];
    }
}