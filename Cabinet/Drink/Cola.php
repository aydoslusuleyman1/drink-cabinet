<?php

namespace Cabinet\Drink;

class Cola extends Drink
{
    public function __construct()
    {
        $this->setName("COCA COLA");
        $this->setSize("33");
        $this->setType("CANNED");
    }
}