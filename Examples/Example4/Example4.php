<?php

namespace Examples\Example4;

use Cabinet\Builder\CabinetBuilder;
use Cabinet\Component\Door;
use Cabinet\DrinkCabinet;
use Cabinet\Schema\CabinetBuildSchema;
use Cabinet\Schema\ShelfBuildSchema;
use Exception;

class Example4
{
    public function play()
    {
        try {
            $cabinetBuildSchema = new CabinetBuildSchema();
            $cabinetBuildSchema->setDoorColor("yellow");
            $cabinetBuildSchema->setDoorStatus(Door::stateMap['open']);

            $shelf1 = new ShelfBuildSchema();
            $shelf1->setColor('red');
            $shelf1->setSize(DrinkCabinet::MAX_DRINK_COUNT_BY_SHELF);

            $shelf2 = new ShelfBuildSchema();
            $shelf2->setColor('grey');
            $shelf2->setSize(DrinkCabinet::MAX_DRINK_COUNT_BY_SHELF);

            $shelf3 = new ShelfBuildSchema();
            $shelf3->setColor('green');
            $shelf3->setSize(DrinkCabinet::MAX_DRINK_COUNT_BY_SHELF);

            $shelf4 = new ShelfBuildSchema();
            $shelf4->setColor('orange');
            $shelf4->setSize(DrinkCabinet::MAX_DRINK_COUNT_BY_SHELF);

            $shelf5 = new ShelfBuildSchema();
            $shelf5->setColor('yellow');
            $shelf5->setSize(DrinkCabinet::MAX_DRINK_COUNT_BY_SHELF);

            $cabinetBuildSchema->setShelfSchemes([$shelf1, $shelf2, $shelf3, $shelf4, $shelf5]);

            $cabinet = (new CabinetBuilder())->build($cabinetBuildSchema);

            echo json_encode($cabinet);

        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }
}



