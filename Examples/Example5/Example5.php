<?php

namespace Examples\Example5;

use Cabinet\Builder\CabinetBuilder;
use Cabinet\Component\Door;
use Cabinet\DrinkCabinet;
use Cabinet\Schema\CabinetBuildSchema;
use Cabinet\Schema\ShelfBuildSchema;
use Exception;

class Example5
{
    public function play()
    {
        try {
            $cabinetBuildSchema = new CabinetBuildSchema();
            $cabinetBuildSchema->setDoorColor("yellow");
            $cabinetBuildSchema->setDoorStatus(Door::stateMap['open']);

            $shelf1 = new ShelfBuildSchema();
            $shelf1->setColor('red');
            $shelf1->setSize(DrinkCabinet::MAX_DRINK_COUNT_BY_SHELF);

            $shelf2 = new ShelfBuildSchema();
            $shelf2->setColor('grey');
            $shelf2->setSize(50);

            $cabinetBuildSchema->setShelfSchemes([$shelf1, $shelf2]);

            $cabinet = (new CabinetBuilder())->build($cabinetBuildSchema);

            echo json_encode($cabinet);

        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }
}



