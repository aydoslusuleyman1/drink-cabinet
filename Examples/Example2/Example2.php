<?php

namespace Examples\Example2;

use Cabinet\Builder\CabinetBuilder;
use Cabinet\Drink\Cola;
use Cabinet\Component\Door;
use Cabinet\DrinkCabinet;
use Cabinet\Process\DrinkLoader;
use Cabinet\Process\DrinkUnLoader;
use Cabinet\Schema\CabinetBuildSchema;
use Cabinet\Schema\ShelfBuildSchema;
use Exception;

class Example2
{
    public function play()
    {
        try {
            $cabinetBuildSchema = new CabinetBuildSchema();
            $cabinetBuildSchema->setDoorColor("yellow");
            $cabinetBuildSchema->setDoorStatus(Door::stateMap['open']);

            $shelf1 = new ShelfBuildSchema();
            $shelf1->setColor('red');
            $shelf1->setSize(DrinkCabinet::MAX_DRINK_COUNT_BY_SHELF);

            $shelf2 = new ShelfBuildSchema();
            $shelf2->setColor('grey');
            $shelf2->setSize(DrinkCabinet::MAX_DRINK_COUNT_BY_SHELF);

            $shelf3 = new ShelfBuildSchema();
            $shelf3->setColor('green');
            $shelf3->setSize(DrinkCabinet::MAX_DRINK_COUNT_BY_SHELF);

            $cabinetBuildSchema->setShelfSchemes([$shelf1, $shelf2, $shelf3]);

            $cabinet = (new CabinetBuilder())->build($cabinetBuildSchema);

            $cocaCola = new Cola();

            $drinkLoader = new DrinkLoader();

            for ($i = 0; $i < 60; $i++) {
                $cabinet = $drinkLoader->load($cabinet, $cocaCola);
            }

            $drinkUnLoader = new DrinkUnLoader();

            for ($i = 0; $i < 30; $i++) {
                $drinkUnLoader->unload($cabinet, $cocaCola);
            }

            echo json_encode($cabinet);

        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }
}



