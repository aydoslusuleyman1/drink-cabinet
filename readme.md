### **DESIGN**

I wrote a simple autoloader to autoload all related classes in one place.

I've tried to use Chain Of Responsibility Pattern for loading and unloading drinks to cabinet.

On our chain processes we're deciding what process is next and automatically continue processes.
 
1. At the beginning it locks load process to avoid another load process's work.
1. Then it's checking door status, if it's closed it will be opened and next process will be triggered.
1. Then its's checking drink types, drink count orderly and after if there is no problem it loads drink to cabinet.
1. In the last point unlock process works and new drinks can be loaded again.

If there is a problem in related process it throws an exception and in catch block it unlocks load 
process, this is a kind of resetting our process.

Also during creation of cabinet the project validates necessary points like max size of shelves and 
max drink capacity of a shelf.